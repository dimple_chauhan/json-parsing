

import UIKit
struct Info: Codable{
    let postId: Int
    let id: Int
    let name: String
    let email: String
    let body: String
    
}

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate{
   
    

   @IBOutlet weak var tableView: UITableView!
     var infoGet = [Info]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       tableView.delegate = self
       tableView.dataSource = self
       let jsonUrl = "https://jsonplaceholder.typicode.com/posts/1/comments"
        guard let url = URL(string: jsonUrl) else { return }
        
        URLSession.shared.dataTask(with: url){  (data,response,error) in
            guard let data = data else { return }
            
            do{
              let info = try JSONDecoder().decode([Info].self, from: data)
                self.infoGet = info
                self.tableView.reloadData()
            }
            catch let jsonerror{
                print("Error: ", jsonerror)
            }
            }.resume()
        
    }
        
    
       
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoGet.count
    }
    
    
    
    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableCell
  
        cell.lblName.text = infoGet[indexPath.row].name
        cell.lblID.text = String(infoGet[indexPath.row].id)
        cell.lblEmail.text = infoGet[indexPath.row].email
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        data.getID = String(infoGet[indexPath.row].id)
        data.getName = infoGet[indexPath.row].name
        data.getEmail = infoGet[indexPath.row].email
        self.navigationController?.pushViewController(data, animated: true)
    }


}

