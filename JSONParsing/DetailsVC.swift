//
//  DetailsVC.swift
//  JSONParsing
//
//  Created by Kapil Dhawan on 19/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {
    var getID: String = ""
    var getName: String = ""
    var getEmail: String = ""
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblID.text = getID
        lblName.text = getName
        lblEmail.text = getEmail
    }
    

  

}
